package eduardoaemunoz.mtgcardfinder.retrofit


import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url
import java.util.concurrent.TimeUnit


// class to retrieve information
data class CardList(val cards : List<Map<String, Any?>>)


// class to hold a card info
class Card (val name : String,
            val type : String,
            val text : String,
            val url : String,
            val id : String)


// backend server
const val URL = "https://api.magicthegathering.io/v1/"


// handle timeout
val client =
    OkHttpClient
        .Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .callTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .build()


// moshi parser
val moshi =
    Moshi.Builder()
         .add(KotlinJsonAdapterFactory())
         .build()


// retrofit connection
val retrofit =
    Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(client)
        .baseUrl(URL)
        .build()


// api interface
interface ApiInterface
{
    @GET
    fun getCards(@Url userQuery : String) : Call<CardList>
}


// api object
object ApiObject
{
    val retrofitService : ApiInterface by lazy { retrofit.create(ApiInterface::class.java) }
}