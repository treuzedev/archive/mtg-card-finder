package eduardoaemunoz.mtgcardfinder


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class SharedViewModel : ViewModel()
{
    // properties
    val card = MutableLiveData<Card>()


    // log changes in card
    fun log()
    {
        Log.i("sharedViewModel log", "card added -> ${card.value!!.name}")
    }
}