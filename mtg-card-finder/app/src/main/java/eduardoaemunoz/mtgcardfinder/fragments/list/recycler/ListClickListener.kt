package eduardoaemunoz.mtgcardfinder.fragments.list.recycler


import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import eduardoaemunoz.mtgcardfinder.MainActivity
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.fragments.list.ListFragment
import eduardoaemunoz.mtgcardfinder.fragments.list.ListFragmentDirections
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class ListClickListener(val holder : ListViewHolder, val card : Card)
{
    // init block
    init
    {
        holder.cardTitle.text = card.name
        holder.cardSubtitle.text = card.type
        holder.binding.cardView.setOnClickListener { onClick() }
    }


    // on click
    fun onClick()
    {
        // update shared view model
        // get main activity reference
        val mainActivity =
            FragmentManager
                .findFragment<ListFragment>(holder.itemView)
                .requireActivity() as MainActivity

        // get shared view model reference
        val sharedViewModel = mainActivity.sharedViewModel
        sharedViewModel.card.value = card

        // navigation
        NavHostFragment
            .findNavController(FragmentManager
                .findFragment(holder
                    .itemView))
            .navigate(ListFragmentDirections
                .actionListFragmentToSingleFragment())

        // log
        sharedViewModel.log()
        Log.i("onClick ClickListener", "navigate with card -> ${card.name}")
    }


    // all images are load to cache
    fun loadImg()
    {
        Glide
            .with(holder.itemView.context)
            .setDefaultRequestOptions(RequestOptions()
                .timeout(60000))
            .load(card.url)
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.cardImg)

        // log
        Log.i("loadImg ClickListener", "img loaded -> ${card.url}")
    }
}