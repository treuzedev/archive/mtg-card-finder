package eduardoaemunoz.mtgcardfinder.fragments.cards.recycler


import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import eduardoaemunoz.mtgcardfinder.MainActivity
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.fragments.cards.CardsFragment
import eduardoaemunoz.mtgcardfinder.fragments.cards.CardsFragmentDirections
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class CardsClickListener(val holder : CardsViewHolder, val card : Card)
{
    // init block
    init { holder.binding.cardView.setOnClickListener { onClick() } }


    // on click card
    fun onClick()
    {
        // update shared view model
        // get main activity reference
        val mainActivity =
            FragmentManager
                .findFragment<CardsFragment>(holder.itemView)
                .requireActivity() as MainActivity

        // get view model reference
        val sharedViewModel = mainActivity.sharedViewModel
        sharedViewModel.card.value = card

        // navigation
        NavHostFragment
            .findNavController(FragmentManager
                .findFragment(holder
                    .itemView))
            .navigate(CardsFragmentDirections
                .actionCardsFragmentToSingle1Fragment())

        // log
        sharedViewModel.log()
        Log.i("onClick ClickListener", "navigate with card -> ${card.name}")
    }


    // load images from cache, or not, and proceed accordingly
    fun loadImg()
    {
        // load image
        Glide
            .with(holder.binding.root)
            .setDefaultRequestOptions(
                RequestOptions()
                .timeout(60000))
            .load(card.url)
            .placeholder(R.drawable.ic_loading)
            .error(R.drawable.ic_error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(holder.cardImg)

        // log
        Log.i("loadImg ClickListener", "img loaded -> ${card.url}")
    }
}