package eduardoaemunoz.mtgcardfinder.fragments.list


import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.FragmentListBinding
import eduardoaemunoz.mtgcardfinder.retrofit.ApiObject
import eduardoaemunoz.mtgcardfinder.retrofit.Card
import eduardoaemunoz.mtgcardfinder.retrofit.CardList
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ListViewModel : ViewModel()
{
    // properties
    lateinit var parentBinding : FragmentListBinding
    lateinit var parentContext : Context
    lateinit var query : String
    val cardList = MutableLiveData<MutableList<Card>>()
    val uiScope = CoroutineScope(Dispatchers.Main)
    val viewModelJob = Job()
    var flag = true


    // api
    fun getData1()
    {
        // if coming from home, fetch api data
        if (flag)
        {
            uiScope.launch { getData2() }
        }

        // if coming from single, hide user warning
        else
        {
            parentBinding.listTextView.visibility = View.GONE
        }
    }

    suspend fun getData2()
    {
        withContext(Dispatchers.IO) { getApiData() }
    }

    fun getApiData()
    {
        // retrofit object, start fetching
        ApiObject
            .retrofitService
            .getCards(query)
            .enqueue(object : Callback<CardList>
            {
                override fun onFailure(call: Call<CardList>, t: Throwable)
                {
                    // log
                    Log.i("getApiData Failure", "${t.message}")

                    // warn user
                    parentBinding.listTextView.text = parentContext.getString(R.string.a)
                }

                override fun onResponse(call: Call<CardList>, response: Response<CardList>)
                {
                    // function variables
                    val list : List<Map<String, Any?>>?

                    // handle weird null events
                    if (response.body() != null)
                    {
                        list = response.body()!!.cards
                    }

                    else
                    {
                        list = listOf()
                        parentBinding.listTextView.visibility = View.VISIBLE
                    }

                    val tmpList = mutableListOf<Card>()
                    var name : String
                    var url : String
                    var type : String
                    var id : String
                    var text : String


                    // update live data
                    for (index in list.indices)
                    {
                        name = list[index]["name"].toString()
                        url = list[index]["imageUrl"].toString()
                        type = list[index]["type"].toString()
                        id = list[index]["id"].toString()
                        text = list[index]["text"].toString()

                        tmpList.add(Card(name, type, text, url, id))
                    }

                    // update ui
                    cardList.value = tmpList

                    // hide warning text
                    parentBinding.listTextView.visibility = View.GONE

                    // make sure data is only loaded when user asks for a search
                    flag = false

                    // log
                    Log.i("getApiData Response", "success! cards retrieved -> ${cardList.value!!.size}")
                }
            })
    }


    // cancel background jobs
    override fun onCleared()
    {
        super.onCleared()
        viewModelJob.cancel()
    }
}