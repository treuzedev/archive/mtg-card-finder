package eduardoaemunoz.mtgcardfinder.fragments.list.single


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import eduardoaemunoz.mtgcardfinder.MainActivity
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.SharedViewModel
import eduardoaemunoz.mtgcardfinder.database.CardDatabase
import eduardoaemunoz.mtgcardfinder.databinding.FragmentSingleBinding
import kotlinx.coroutines.InternalCoroutinesApi


class SingleFragment : Fragment()
{
    // binding object
    lateinit var binding : FragmentSingleBinding


    // shared view model
    lateinit var sharedViewModel : SharedViewModel


    // view model
    lateinit var viewModel : SingleViewModel


    // lifecycle fun
    @InternalCoroutinesApi
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // binding object
        binding =
            DataBindingUtil
                .inflate(inflater,
                    R.layout.fragment_single,
                    container,
                    false)


        // shared view model
        val owner = activity as MainActivity
        sharedViewModel =
            ViewModelProvider(owner)
            .get(SharedViewModel::class.java)
        binding.sharedViewModel = sharedViewModel


        // view model
        viewModel =
            ViewModelProvider(this)
            .get(SingleViewModel::class.java)
        binding.singleViewModel = viewModel
        viewModel.sharedViewModel = sharedViewModel
        viewModel.parentBinding = binding
        viewModel.parentContext = requireContext()
        viewModel.parentFragment = this


        // ui update
        viewModel.loadImg()


        // database
        viewModel.dao = CardDatabase.getInstance(requireContext()).dao


        // return
        return binding.root
    }
}
