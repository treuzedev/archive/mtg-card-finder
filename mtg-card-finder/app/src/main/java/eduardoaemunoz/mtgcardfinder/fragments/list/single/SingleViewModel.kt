package eduardoaemunoz.mtgcardfinder.fragments.list.single


import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.SharedViewModel
import eduardoaemunoz.mtgcardfinder.database.CardEntity
import eduardoaemunoz.mtgcardfinder.database.DatabaseDao
import eduardoaemunoz.mtgcardfinder.databinding.FragmentSingleBinding
import kotlinx.coroutines.*


class SingleViewModel : ViewModel()
{
    // properties
    lateinit var sharedViewModel: SharedViewModel
    lateinit var parentContext : Context
    lateinit var parentBinding : FragmentSingleBinding
    lateinit var parentFragment : SingleFragment
    lateinit var dao : DatabaseDao
    val entity = CardEntity()
    val uiScope = CoroutineScope(Dispatchers.Main)
    val viewModelJob = Job()


    // all images are load to cache
    fun loadImg()
    {
        // load images
        Glide
            .with(parentContext)
            .setDefaultRequestOptions(
                RequestOptions()
                .timeout(60000))
            .load(sharedViewModel.card.value!!.url)
            .error(R.drawable.ic_error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(parentBinding.cardImg)

        // log
        Log.i("loadImg ClickListener", "img loaded -> ${parentBinding.cardImg.id}")
    }


    // back button
    fun btnBackClick()
    {
        // navigate back
        NavHostFragment.findNavController(parentFragment).navigateUp()
    }


    // save button
    fun btnSaveClick()
    {
        // update data to add
        updateEntity()

        // save card to database
        databaseWork1()

        // warn user
        Toast
            .makeText(parentContext,
                "Card saved!",
                Toast.LENGTH_LONG)
            .show()
    }


    // fun update entity
    fun updateEntity()
    {
        // variables
        val card = sharedViewModel.card.value!!

        // update entity
        entity.name = card.name
        entity.type = card.type
        entity.text = card.text
        entity.url = card.url
        entity.cardId= card.id
    }


    // database work in background
    fun databaseWork1()
    {
        uiScope.launch { databaseWork2() }
    }

    suspend fun databaseWork2()
    {
        withContext(Dispatchers.IO) {

            // database work
            dao.addCard(entity)

            // log
            Log.i("btnSaveClick SingleVM", "card saved -> ${entity.name}")}
    }


    // cancel background jobs
    override fun onCleared()
    {
        super.onCleared()
        viewModelJob.cancel()
    }
}