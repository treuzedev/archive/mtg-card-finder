package eduardoaemunoz.mtgcardfinder.fragments.list


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.databinding.FragmentListBinding
import eduardoaemunoz.mtgcardfinder.fragments.list.recycler.ListAdapter


class ListFragment : Fragment()
{
    // binding object
    lateinit var binding : FragmentListBinding


    // view model
    lateinit var viewModel : ListViewModel


    // args
    lateinit var args : ListFragmentArgs


    // lifecycle fun
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // binding object
        binding = DataBindingUtil
            .inflate(inflater,
                    R.layout.fragment_list,
                    container,
                    false)


        // args
        args = ListFragmentArgs.fromBundle(requireArguments())


        // view model
        viewModel =
            ViewModelProvider(this)
            .get(ListViewModel::class.java)
        viewModel.parentBinding = binding
        viewModel.parentContext = this.requireContext()
        viewModel.query = args.query
        viewModel.getData1()


        // recycler view
        val adapter = ListAdapter()
        val manager = FlexboxLayoutManager(requireContext())
        manager.justifyContent = JustifyContent.CENTER
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = manager


        // update recycler view contents
        viewModel
            .cardList
            .observe(viewLifecycleOwner,
                Observer { it?.let { adapter.submitList(it) } })


        // return fragment
        return binding.root
    }
}
