package eduardoaemunoz.mtgcardfinder.fragments.cards


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import eduardoaemunoz.mtgcardfinder.R
import eduardoaemunoz.mtgcardfinder.database.CardDatabase
import eduardoaemunoz.mtgcardfinder.databinding.FragmentCardsBinding
import eduardoaemunoz.mtgcardfinder.fragments.cards.recycler.CardsAdapter
import kotlinx.coroutines.InternalCoroutinesApi


class CardsFragment : Fragment()
{
    // binding object
    lateinit var binding : FragmentCardsBinding


    // view model
    lateinit var viewModel : CardsViewModel


    // lifecycle fun
    @InternalCoroutinesApi
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // binding object
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cards, container, false)


        // view model
        viewModel =
            ViewModelProvider(this)
            .get(CardsViewModel::class.java)
        viewModel.parentBinding = binding
        binding.cardsViewModel = viewModel


        // database
        viewModel.dao = CardDatabase.getInstance(requireContext()).dao
        viewModel.loadCards()


        // recycler view
        val adapter =
            CardsAdapter()
        val manager = FlexboxLayoutManager(requireContext())
        manager.justifyContent = JustifyContent.CENTER
        manager.flexWrap = FlexWrap.WRAP
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = manager


        // update view model contents
        viewModel
            .uiCardList
            .observe(viewLifecycleOwner,
                     Observer { it?.let { adapter.submitList(it) } })


        // return fragment
        return binding.root
    }
}
