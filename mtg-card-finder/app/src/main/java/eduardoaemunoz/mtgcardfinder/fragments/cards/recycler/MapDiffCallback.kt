package eduardoaemunoz.mtgcardfinder.fragments.cards.recycler


import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import eduardoaemunoz.mtgcardfinder.retrofit.Card


class MapDiffCallback : DiffUtil.ItemCallback<Card>()
{
    override fun areItemsTheSame(oldItem: Card,
                                 newItem: Card): Boolean
    {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Card,
                                    newItem: Card): Boolean
    {
        return oldItem == newItem
    }
}