package eduardoaemunoz.mtgcardfinder.fragments.home


import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.NavHostFragment
import eduardoaemunoz.mtgcardfinder.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlin.random.Random
import kotlin.random.nextInt


class HomeViewModel : ViewModel()
{
    // properties
    lateinit var parentBinding : FragmentHomeBinding
    lateinit var parentContext : Context
    lateinit var parentFragment : HomeFragment
    lateinit var query : String


    // clickable btn
    fun click()
    {
        getUserQuery()
        NavHostFragment
            .findNavController(parentFragment)
            .navigate(HomeFragmentDirections
                .actionHomeFragmentToListFragment(query))
    }


    // get information
    fun getUserQuery()
    {
        // function variables
        val cardName = parentBinding
            .inputCardName
            .text?.toString()
        val cardType = parentBinding
            .cardType.inputCardType
            .text?.toString()


        // create query string
        val tmp = mutableListOf<String>()

        if (!cardName.isNullOrBlank() && !cardName.isNullOrEmpty())
        {
            tmp.add("name=$cardName&")
        }

        if (!cardType.isNullOrBlank() && !cardType.isNullOrEmpty())
        {
            tmp.add("type=$cardType")
        }


        // if no input is provided
        if (tmp.isNullOrEmpty())
        {
            // warn user
            Toast
                .makeText(parentContext,
                    "No filters used, fetching random cards..",
                    Toast.LENGTH_LONG).show()

            // get random number to retrieve data
            val random = Random.nextInt(1..101)
            tmp.add("number=$random")
        }


        // create string and pass it to property
        val userQuery = tmp.joinToString("")

        // query needs full url to work
        query = "https://api.magicthegathering.io/v1/cards?$userQuery"

        // log
        Log.i("getUserQuery", "query -> $query")
    }
}