package eduardoaemunoz.mtgcardfinder.fragments.cards


import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eduardoaemunoz.mtgcardfinder.database.CardEntity
import eduardoaemunoz.mtgcardfinder.database.DatabaseDao
import eduardoaemunoz.mtgcardfinder.databinding.FragmentCardsBinding
import eduardoaemunoz.mtgcardfinder.retrofit.Card
import kotlinx.coroutines.*


class CardsViewModel : ViewModel()
{
    // properties
    lateinit var dao : DatabaseDao
    var cardList = listOf<CardEntity>()
    val uiCardList = MutableLiveData<MutableList<Card>>()
    val uiScope = CoroutineScope(Dispatchers.Main)
    val viewModelJob = Job()
    lateinit var parentBinding : FragmentCardsBinding


    // load cards
    fun loadCards()
    {
        uiScope.launch { getData() }
    }

    suspend fun getData()
    {
        // get cards from database
        cardList =
            withContext(Dispatchers.IO)
            {
                // get cards
                val list = dao.getAllCards()

                return@withContext list
            }

        // log
        Log.i("loadCards CardsVM", "cards loaded -> ${cardList.size}")

        // update ui
        updateUi()
    }


    // update recycler view
    fun updateUi()
    {
        // variables
        val tmp = mutableListOf<Card>()

        // transform
        for (index in cardList.indices)
        {
             tmp.add(Card(
                 cardList[index].name,
                 cardList[index].type,
                 cardList[index].text,
                 cardList[index].url,
                 cardList[index].id.toString()))
        }

        // update
        uiCardList.value = tmp

        // hide warning text
        parentBinding.cardsTextView.visibility = View.GONE
    }


    // delete button, database work
    fun btnDeleteAll()
    {
        uiScope.launch { databaseWork() }
    }

    suspend fun databaseWork()
    {
        withContext(Dispatchers.IO)
        {
            // database work
            dao.deleteAllCards()

            loadCards()

            // log
            Log.i("btnDeleteAll CardsVM", "all cards deleted!")
        }
    }


    // cancel background work
    override fun onCleared()
    {
        super.onCleared()
        viewModelJob.cancel()
    }
}