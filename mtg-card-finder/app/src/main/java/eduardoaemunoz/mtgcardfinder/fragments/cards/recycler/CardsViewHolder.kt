package eduardoaemunoz.mtgcardfinder.fragments.cards.recycler


import androidx.recyclerview.widget.RecyclerView
import eduardoaemunoz.mtgcardfinder.databinding.CardDeckViewBinding


class CardsViewHolder(val binding : CardDeckViewBinding)
    : RecyclerView.ViewHolder(binding.root)
{
    val cardImg = binding.cardView
}