package eduardoaemunoz.mtgcardfinder.database


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "userCardsTable")
data class CardEntity(

    @PrimaryKey(autoGenerate = true)
    var id : Int = 0,

    @ColumnInfo(name = "cardName")
    var name : String = "card name",

    @ColumnInfo(name = "cardType")
    var type : String = "card type",

    @ColumnInfo(name = "cardText")
    var text : String = "card text",

    @ColumnInfo(name = "url")
    var url : String = "card image url",

    @ColumnInfo(name = "cardId")
    var cardId : String = "card id"
)